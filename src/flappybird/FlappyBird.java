/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flappybird;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author sinhads
 */
public class FlappyBird {

    // Global Variables
    private static final int GAME_PANEL_WIDTH = 600;
    private static final int GAME_PANEL_HEIGHT = 600;
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("Flappy Bird");
        GamePanel gamePanel = new GamePanel(GAME_PANEL_WIDTH, GAME_PANEL_HEIGHT);
        
        ArrayList<KeyAdapter> gamePanelKA = gamePanel.getKeyAdapters();
        mainFrame.add(gamePanel);
        for (int i = 0; i < gamePanelKA.size(); i ++) {
            mainFrame.addKeyListener(gamePanelKA.get(i));
        }
        mainFrame.addKeyListener(new keyListener());
        
        mainFrame.setSize(GAME_PANEL_WIDTH, GAME_PANEL_HEIGHT);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }
    
    
    
    
    private static class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                System.exit(0);
            }
        }
    }
    
}
