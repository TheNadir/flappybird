/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flappybird;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * @author sinhads
 */
public class Player extends Rectangle {
    
    // Global Variables
    private final double ANGLE = 30.0;
    private final int ACCELERATION = 2;
    
    private int gravity = -2;
    private int speed = 5;
    private double xScale = Math.cos(ANGLE);
    private double yScale = Math.sin(ANGLE);
    private int xVelocity = 0;
    private int yVelocity = 0;
    
    
    
    
    public Player(int x, int y, int width, int height, Color color) {
        super(x, y, width, height, color);
    }
    
    
    
    
    public void move() {
        //speed += ACCELERATION;
        yVelocity -= gravity - speed;
        xVelocity = 10;
        
        //setX(getX() + xVelocity);
        setY(getY() + yVelocity);
    }
    
    public void jump() {
        yVelocity = gravity - 30;
    }
    
    
    
    
    
    
    // Getters and Setters
    public keyListener getKeyListener() {
        return new keyListener();
    }
    
    public void display() {
        System.err.println("Velocity X: " + xVelocity);
        System.err.println("Velocity Y: " + yVelocity);
    }
    
    
    
    
    // Getters and Setters
    public void setSpeed(int newSpeed) {
        speed = newSpeed;
    }
    
    public void setGravity(int newGravity) {
        gravity = newGravity;
    }
    
    
    
    
    // Key Listener
    private class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
                jump();
            }
        }
    }
}
