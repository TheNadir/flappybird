/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flappybird;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 *
 * @author sinhads
 */
public class Text extends Element {
    
    // Global Variables
    private String text;
    
    
    
    public Text(int x, int y, int width, int height, String newText) {
        super(x, y, width, height);
        text = newText;
    }
    
    
    
    @Override
    public void draw(Graphics g) {
        Font font = new Font("Arial", Font.PLAIN, 50);
        g.setFont(font);
        g.setColor(Color.BLUE);
        g.drawString(text, getX(), getY());
        
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.BLUE);
        //g2d.drawString(getText(), getX(), getY());
    }
    
    
    
    // Getters and Setters
    public String getText() {
        return text;
    }
    public void setText(String newText) {
        text = newText;
    }
}
