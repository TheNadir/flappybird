/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flappybird;

import commandFrame.CmdFrame;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JPanel;

/**
 *
 * @author sinhads
 */
public class GamePanel extends JPanel implements Runnable {
    
     // Global Variables
    private final int PANEL_WIDTH;
    private final int PANEL_HEIGHT;
    private int FPS = 10;
    private final Pipe pipe = new Pipe();
    
    private boolean pauseGame = true;
    private ArrayList<Element> elements = new ArrayList<>();
    private Player player;
    private ArrayList<Rectangle> pipes = new ArrayList<>();
    private ArrayList<Element> overlay = new ArrayList<>();
    private int score = 0;
    private Text scoreLbl;
    private boolean resetTime = true;
    
    private CmdFrame cmdFrame = new CmdFrame();
    private int pipeSpeed = 2;
    
    
    
    
    
    public GamePanel(int panelWidth, int panelHeight) {
        PANEL_WIDTH = panelWidth;
        PANEL_HEIGHT = panelHeight;
        
        
        pauseGame = true;
        startGame();
        
        
        new Thread(this).start();
    }
    
    
    
    
    @Override
    public void run() {
        long tm = System.currentTimeMillis();
        int timer = 0;
        int pipesTimer = -150;
        
        while(true) {
            System.out.println(pauseGame);
            while (!(pauseGame)) {
                if (resetTime) {
                    tm = System.currentTimeMillis();
                    resetTime = false;
                }
                repaint();
                update();



                // work the player physics
                if (timer%10 == 0) {
                    player.move();
                    timer = 1;
                }

                // Spawn Pipes
                if (pipesTimer%150 == 0) {
                    int[] yCoords = pipe.getRandomPipe();
                    pipes.add(new Rectangle(PANEL_WIDTH, -1000, pipe.getPipeWidth(), yCoords[0] + 1000, 
                                            Color.GREEN));
                    pipes.add(new Rectangle(PANEL_WIDTH, yCoords[1], 
                                            pipe.getPipeWidth(), PANEL_HEIGHT + 1000, 
                                            Color.GREEN));
                    elements.add(new Rectangle(PANEL_WIDTH + pipe.getPipeWidth(), 0, 
                                                10, PANEL_HEIGHT, Color.BLACK));
                    pipesTimer = 1;
                }



                // player collision with pipes
                for (int i = 0; i < pipes.size(); i ++) {
                    if (pipes.get(i).isColliding(player)) {
                        endGame();
                    }
                }
                
                // player collides with invisible score box
                for (int i = 1; i < elements.size(); i ++) {
                    if (elements.get(i).isColliding(player)) {
                        score ++;
                        elements.remove(i);
                        i --;
                    }
                }
                
                // player goes above -1000 or below [PANEL_WIDTH] + 1000 -> end game
                if (player.getY() <= -1000 || player.getY() >= PANEL_WIDTH + 1000) {
                    endGame();
                }
                



                try {
                    tm += FPS;
                    Thread.sleep(Math.max(0, tm - System.currentTimeMillis()));
                    timer ++;
                    pipesTimer ++;
                } catch (InterruptedException ex) {
                    System.err.println("Interrupted Exception");
                    ex.printStackTrace();
                }
            }
        }
    }
    
    @Override
    public void paint(Graphics g) {
        Iterator elementsIterator = elements.iterator();
        Iterator pipesIterator = pipes.iterator();
        Iterator overlayIterator = overlay.iterator();
        
        while (elementsIterator.hasNext()) {
            ((Element) elementsIterator.next()).draw(g);
        }
        
        while (pipesIterator.hasNext()) {
            ((Element) pipesIterator.next()).draw(g);
        }
        
        player.draw(g);
        scoreLbl.draw(g);
        
        while (overlayIterator.hasNext()) {
            ((Element) overlayIterator.next()).draw(g);
        }
    }
    
    public void update() {
        setCmdPreferences();
        
        scoreLbl.setText(String.valueOf(score));
        
        for (int i = 0; i < pipes.size(); i ++) {
            pipes.get(i).setX(pipes.get(i).getX() - pipeSpeed);
            
            // remove pipe when it goes off the screen
            if (pipes.get(i).getX() + pipes.get(i).getWidth() < 0) {
                pipes.remove(i);
                i = 0;
            }
        }
        
        for (int i = 1; i < elements.size(); i ++) {
            elements.get(i).setX(elements.get(i).getX() - 2);
            
            if (elements.get(i).getX() + elements.get(i).getWidth() < 0) {
                elements.remove(i);
                i = 1;
            }
        }
    }
    
    public void startGame() {
        player = new Player(PANEL_WIDTH/2-20, PANEL_HEIGHT/2-20, 40, 40, Color.WHITE);
        removeOverlay();
        overlay.add(new Text(50, (PANEL_HEIGHT/2) - 25, 0, 0, "Press SPACE to start."));
        repaint();
        
        
        elements = new ArrayList<>();
        pipes = new ArrayList<>();
        score = 0;
        scoreLbl = new Text(30, 60, 0, 0, String.valueOf(score));
        
        
        // initialize background
        elements.add(new Rectangle(0, 0, PANEL_WIDTH, PANEL_HEIGHT, Color.BLACK));
        // initialize player
        //player = new Player(0, 0, 40, 40, Color.WHITE);
//        player = new Player(PANEL_WIDTH/2-20, PANEL_HEIGHT/2-20, 40, 40, Color.WHITE);
    }
    
    public void endGame() {
        pauseGame = true;
        overlay.add(new Text(160, (PANEL_HEIGHT/2) - 25, 0, 0, "YOU FAIL!!!"));
        overlay.add(new Text(20, (PANEL_HEIGHT/2) + 35, 0, 0, "(Press SPACE to restart)"));
        
        elements = new ArrayList<>();
        pipes = new ArrayList<>();
        
        repaint();
    }
    
    public void removeOverlay() {
        overlay = new ArrayList<>();
    }
    
    public void setCmdPreferences() {
        if (cmdFrame == null) {
            cmdFrame = new CmdFrame();
        }
        
        System.err.println("FPS:        " + cmdFrame.getFPS());
        System.err.println("Speed:      " + cmdFrame.getSpeed());
        System.err.println("Gravity:    " + cmdFrame.getGravity());
        System.err.println("Pipe Speed: " + cmdFrame.getPipeSpeed());
        
        FPS = cmdFrame.getFPS();
        player.setSpeed(cmdFrame.getSpeed());
        player.setGravity(cmdFrame.getGravity());
        pipeSpeed = cmdFrame.getPipeSpeed();
    }
    
    public ArrayList<KeyAdapter> getKeyAdapters() {
        ArrayList<KeyAdapter> ret = new ArrayList<>();
        ret.add(player.getKeyListener());
        ret.add(new keyListener());
        return ret;
    }
    
    
    
    
    
    private class keyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent ke) {
            if (pauseGame) {
                if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
                    pauseGame = false;
                    resetTime = true;
                    startGame();
                    removeOverlay();
                    player.jump();
                    System.err.println("(" + player.getX() + ", " + player.getY() + ")");
                }
            }
            
            player.jump();
            
            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                System.err.println("(" + player.getX() + ", " + player.getY() + ")");
            }
            
            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                cmdFrame.setVisible(true);
            }
        }
    }
    
}
