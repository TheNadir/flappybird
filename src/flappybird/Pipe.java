/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flappybird;

import java.util.Random;

/**
 *
 * @author sinhads
 */
public class Pipe {
    
    // Global Variables
    private final int pipeWidth = 100;
    
    private int[][] yCoords;
    
    
    
    
    public Pipe() {
        yCoords = new int[5][2];
        
        yCoords[0][0] = 225;
        yCoords[0][1] = 375;
        yCoords[1][0] = 150;
        yCoords[1][1] = 300;
        yCoords[2][0] = 300;
        yCoords[2][1] = 450;
        yCoords[3][0] = 100;
        yCoords[3][1] = 250;
        yCoords[4][0] = 350;
        yCoords[4][1] = 500;
    }
    
    
    
    
    public int[] getRandomPipe() {
        int[] ret = new int[2];
        
        int rnd = getRandomNum(0, yCoords.length - 1);
        ret = yCoords[rnd];
        
        return ret;
    }
    
    private int getRandomNum(int lowBound, int upBound) {
        Random rnd = new Random();
        return rnd.nextInt(upBound - lowBound + 1) + lowBound;
    }
    
    
    
    
    // Getters and Setters
    public int getPipeWidth() {
        return pipeWidth;
    }
    
}
