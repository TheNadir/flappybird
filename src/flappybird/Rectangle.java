/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flappybird;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author sinhads
 */
public class Rectangle extends Element {
    
    // Global Variables
    Color color;
    
    
    
    public Rectangle(int x, int y, int width, int height, Color newColor) {
        super(x, y, width, height);
        color = newColor;
    }
    
    
    
    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillRect(getX(), getY(), getWidth(), getHeight());
    }
    
    
    
    
    // Getters and Setters
    public Color getColor() {
        return color;
    }
    public void setColor(Color newColor) {
        color = newColor;
    }
}
