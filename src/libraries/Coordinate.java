/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraries;

/**
 *
 * @author sinhads
 */
public class Coordinate {
    
    // Global Variables
    int x, y;
    
    
    
    private Coordinate(int newX, int newY) {
        x = newX;
        y = newY;
    }
    
    
    
    
    // Global Variables
    private int getX() {
        return x;
    }
    private void setX(int newX) {
        x = newX;
    }
    
    private int getY() {
        return y;
    }
    private void setY(int newY) {
        y = newY;
    }
}
